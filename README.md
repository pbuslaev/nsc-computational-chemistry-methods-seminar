# NSC computational chemistry methods seminar
 
This is a page related to CompChem seminars at the Nanoscience center. At our research center, we have a lot of people performing computational research and we think it is important to build a stronger community of computational scientists at NSC. Our seminars are aiming to do so. Once a week we are planning to share our experiences and discuss seminal or novel methodological aspects of computational chemistry and biology with our colleagues in a relaxed and friendly atmosphere. At the seminars, we are going to cover the methodological aspects of MD and QM simulations. We will discuss both well-established best practices in the field as well as some most recent progress. We believe that open discussion during the seminars will lead to new research collaborations and even better science conducted at NSC.
 
The first seminars are dedicated to touch base and better understand what different groups at the NSC are doing. We planned first 5 meetings, where we are planning to discuss various methods widely used at NSC. All the meetings are on Thursdays at 15:00 in 121 YNC. The approximate schedule is:

- 27.10 15:00 Gerrit Groenhof will discuss quantum calculations in chemistry
- 10.11 15:00 Marko Melander will discuss DFT calculations
- 24.11 15:00 Buslaev Pavel will discuss Classical MD simulations
- 08.12 15:00 Karoliina Honkala will discuss Monte-Carlo simulations
- 15.12 15:00 Antti Pihlajamäki will discuss Machine Learning approaches to chemistry

After these first 5 seminars we will have a short break and we will return back in January 2023. We kindly encourage you to present what you are interested in and suggest new topics for the discussion (just leave a comment [here](https://gitlab.com/pbuslaev/nsc-computational-chemistry-methods-seminar/-/issues/1)). See you all at the seminars.

 
