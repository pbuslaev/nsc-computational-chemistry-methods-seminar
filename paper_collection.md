# Proposed papers for discussion

- [Generalized-ensemble algorithms: enhanced sampling techniques for Monte Carlo and molecular dynamics simulations](https://doi.org/10.1016/j.jmgm.2003.12.009)
- [The calculation of the potential of mean force using computer simulations](https://doi.org/10.1016/0010-4655(95)00053-I)
- [Introduction to QM/MM Simulations](https://doi.org/10.1007/978-1-62703-017-5_3)
- [Markov models of molecular kinetics: Generation and validation](https://doi.org/10.1063/1.3565032)
